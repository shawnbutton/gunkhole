// https://docs.cypress.io/api/table-of-contents

describe('Home Page', () => {
  it('Contains a title and tabs', () => {
    cy.visit('/')
    cy.contains('Wie Der Wind Checklists')

    cy.contains('Launching')
    cy.contains('Docking')
    cy.contains('Leaving')
  })

  it('Switches between lists when you click on a tab', () => {
    cy.visit('/')

    cy.contains('Launching').click()
    cy.contains('Monitor stowed')

    cy.contains('Docking').click()
    cy.contains('No lines trailing')

    cy.contains('Leaving').click()
    cy.contains('Flag put away')
  })
})
